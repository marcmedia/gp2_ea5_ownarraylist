
import java.util.Arrays;



/**
 * 
 * @author Marc
 *
 * @param <E>
 */
public class MeineArrayList<E> {

	private E[] list;
	private int size = 0;
	private int capacity;
	private int increment;
	
	private final int DEFAULT_CAPACITY = 16;
	private final int INITIAL_CAPACITY = 2;
	private final int INCREMENT = 10;
	
	
	/**
	 * 
	 */
	public MeineArrayList() {
		list = (E[]) new Object[DEFAULT_CAPACITY];
	}
	
	
	/**
	 * constructs a MeineArrayList-Object with initial capacity capacity
	 * and increment increment
	 * @param capacity the initial capacity
	 * @param increment the number of positions that will be added every time
	 * the list is made bigger
	 */
	public MeineArrayList(final int capacity, final int increment) {
		this.capacity = capacity;
		list = (E[]) new Object[capacity];
		size = 0;
		this.increment = increment;
	}


	/**
	 * if there is a free index in the array to add the element given as a parameter it will be added to the array.
	 * if not, the first thing to do is in to enlarge the array  by the increment value than the item will be added.
	 * @param item
	 */
	public void addElement(final E item) {
		if (size < list.length) {
			list[size] = item;
			size++;
		} else {
			list = Arrays.copyOf(list, list.length + increment);
			list[size++] = item;
		}
	}

	
	/**
	 * deletes the element with the index given as a parameter value
	 * @param index
	 */
	public void deleteElement(final int index) {
		for (int i = index; i < size - 1; i++) {
			list[i] = list[i + 1];
		}
		list[size - 1] = null;
		--size;
	}

	
	/**
	 * 
	 * returns the number of elements currently existing in the list
	 * @return
	 */
	public int numberOfElements() {
		return this.size;
	}

	
	/**
	 * returns the current size of the list
	 * @return
	 */
	public int sizeOfCapacity() {
		return list.length;
	}

	
	/**
	 * 
	 * returns the item of the index given as a parameter
	 * @param index
	 * @return
	 */
	public E getElement(final int index) {
		return list[index];
	}
	
	
	/**
	 * 
	 * @param elementName
	 * @return
	 */
	public boolean existsInList(final String elementName) {
		boolean exists = false;
		for (int i = 0; i < list.length; i++) {
			if (list[i].equals(elementName)) {
				exists = true;
			}
		}
		return exists;
	}
	

	/**
	 * delete all Items from the lists
	 */
	public void deleteAllElements() {
		list = (E[]) new Object[this.capacity];
	}


	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		MeineArrayList<String> liste = new MeineArrayList<String>(2, 10);
		liste.addElement("Erika");
		liste.addElement("Marc");
		liste.addElement("Mathias");
		liste.addElement("Keli");
		liste.addElement("Sina");
		liste.addElement("Manuela");
		liste.addElement("Kerstin");
		liste.addElement("Julia");
		liste.addElement("Suzanna");	
		
		
		for (int i = 0; i < liste.numberOfElements(); i++) {
			System.out.println(i + " " + liste.getElement(i));
		}
		
		System.out.println("----------- delete Element -------------------");
		
		liste.deleteElement(5);
		liste.deleteElement(6);
		liste.deleteElement(1);
		
		for (int i = 0; i < liste.numberOfElements(); i++) {
			System.out.println(i + " " + liste.getElement(i));
		}

	}
}

